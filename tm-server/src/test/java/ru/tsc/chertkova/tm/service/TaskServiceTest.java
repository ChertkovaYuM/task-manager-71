package ru.tsc.chertkova.tm.service;

import org.junit.experimental.categories.Category;
import ru.tsc.chertkova.tm.marker.DataBaseCategory;

@Category(DataBaseCategory.class)
public class TaskServiceTest {
//
//    @Nullable
//    private static IPropertyService propertyService;
//
//    @Nullable
//    private static IConnectionService connectionService;
//
//    @NotNull
//    private final TaskRepository taskRepository = new TaskRepository();
//
//    @NotNull
//    private final UserRepository userRepository = new UserRepository();
//
//    @NotNull
//    private final IUserService userService = new UserService(propertyService, userRepository);
//
//    @NotNull
//    private final ITaskService service = new TaskService(taskRepository);
//
//    @Rule
//    @NotNull
//    public final ExpectedException thrown = ExpectedException.none();
//
//    @BeforeClass
//    public static void initConnectionService() {
//        propertyService = new PropertyService();
//    }
//
//    @AfterClass
//    public static void destroy() {
//        connectionService.close();
//    }
//
//    @Before
//    public void init() {
//        for (Task t :
//                TASK_LIST) {
//            taskRepository.add(t);
//        }
//    }
//
//    @After
//    public void end() {
//        taskRepository.clear();
//    }
//
//    @Test
//    public void findAll() {
//        Assert.assertEquals(TASK_LIST, service.findAll(ADMIN1.getId()));
//    }
//
//    @Test
//    public void FindAllByUserId() {
//        Assert.assertEquals(ADMIN1_TASK_LIST, service.findAll(ADMIN1.getId()));
//        thrown.expect(UserIdEmptyException.class);
//        service.findAll(ADMIN1.getId());
//    }
//
//    @Test
//    public void clear() {
//        service.clear(USER1.getId());
//        Assert.assertTrue(taskRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void clearByUserId() {
//        service.clear(USER1.getId());
//        Assert.assertTrue(taskRepository.findAll(USER1.getId()).isEmpty());
//        thrown.expect(UserIdEmptyException.class);
//        service.clear(null);
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final List<Task> list = new ArrayList<>(taskRepository.findAll());
//        @Nullable final Task removed = service.remove(USER1.getId(), USER1_TASK2);
//        Assert.assertEquals(USER1_TASK2, removed);
//        list.remove(USER1_TASK2);
//        Assert.assertEquals(list, taskRepository.findAll());
//        Assert.assertNull(service.remove(USER1.getId(), null));
//        Assert.assertNull(service.remove(USER1.getId(), USER1_TASK2));
//    }
//
//    @Test
//    public void removeByUserId() {
//        @NotNull final List<Task> list = new ArrayList<>(taskRepository.findAll());
//        service.remove(USER1.getId(), USER1_TASK2);
//        list.remove(USER1_TASK2);
//        Assert.assertEquals(list, taskRepository.findAll());
//        service.remove(USER1.getId(), null);
//        Assert.assertEquals(list, taskRepository.findAll());
//        service.remove(USER1.getId(), USER2_TASK1);
//        Assert.assertEquals(list, taskRepository.findAll());
//        thrown.expect(UserIdEmptyException.class);
//        service.remove(null, USER2_TASK1);
//        Assert.assertEquals(list, taskRepository.findAll());
//    }
//
//    @Test
//    public void createByNameAndUserId() {
//        taskRepository.clear();
//        @NotNull final Task expected = new Task();
//        expected.setName(USER1_TASK1.getName());
//        expected.setUser(USER1);
//        @NotNull final Task created = taskRepository.findAll().get(0);
//        thrown.expect(UserIdEmptyException.class);
//    }
//
//    @Test
//    public void createByNameAndUserIdAndDescription() {
//        taskRepository.clear();
//        @NotNull final Task expected = new Task();
//        expected.setName(USER1_TASK1.getName());
//        expected.setDescription(USER1_TASK1.getDescription());
//        expected.setUser(USER1);
//        @NotNull final Task created = taskRepository.findAll().get(0);
//        thrown.expect(UserIdEmptyException.class);
//    }

}
