package ru.tsc.chertkova.tm.integration.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;

import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.chertkova.tm.marker.WebCategory;
import ru.tsc.chertkova.tm.model.dto.TaskDTO;
import ru.tsc.chertkova.tm.model.dto.Result;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(WebCategory.class)
public class TaskRestEndpointClientTest {

    @NotNull
    private static final String API_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @NotNull
    private static String SESSION_ID;

    @NotNull
    private TaskDTO task_1;

    @NotNull
    private TaskDTO task_2;

    @NotNull
    private TaskDTO task_3;

    @NotNull
    private TaskDTO task_4;

    @BeforeClass
    @SneakyThrows
    public static void setUpClass() {
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final String authUrl = "http://localhost:8080/api/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response = template.getForEntity(authUrl, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders responseHeaders = response.getHeaders();
        final List<HttpCookie> cookies =
                HttpCookie.parse(responseHeaders.getFirst(HttpHeaders.SET_COOKIE));
        SESSION_ID = cookies.stream()
                .filter(httpCookie -> "JSESSIONID".equals(httpCookie.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(SESSION_ID);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + SESSION_ID));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<TaskDTO> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity entity
    ) {
        @NotNull final RestTemplate template = new RestTemplate();
        return template.exchange(url, method, entity, TaskDTO.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
    }

    @Before
    public void setUp() {
        @NotNull final String url = API_URL + "save";
        task_1 = new TaskDTO();
        task_2 = new TaskDTO();
        task_3 = new TaskDTO();
        task_4 = new TaskDTO();
        task_1.setName("TASK_1");
        task_2.setName("TASK_2");
        task_3.setName("TASK_3");
        task_4.setName("TASK_4");
        sendRequest(url, HttpMethod.POST, new HttpEntity<TaskDTO>(task_1, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<TaskDTO>(task_2, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<TaskDTO>(task_3, header));

    }

    @After
    public void tearDown() throws Exception {
        @NotNull final String clearUrl = API_URL + "clear";
        sendRequest(clearUrl, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @Test
    public void create() {
        @NotNull final String url = API_URL + "create";
        @NotNull final ResponseEntity<TaskDTO> response = sendRequest(
                url,
                HttpMethod.POST,
                new HttpEntity<>(header)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @NotNull final TaskDTO taskDto = response.getBody();
        Assert.assertNotNull(taskDto);
    }

    @Test
    public void existsById() {
        @NotNull final String taskId = task_1.getId();
        @NotNull final String url = API_URL + "existsById/" + taskId;
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<Result> response = template.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(header),
                Result.class);
        Assert.assertNotNull(response.getBody());
        boolean exists = response.getBody().getSuccess();
        Assert.assertTrue(exists);
    }

    @Test
    public void findById() {
        @NotNull final String taskId = task_1.getId();
        @NotNull final String url = API_URL + "findById/" + taskId;
        @NotNull final ResponseEntity<TaskDTO> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDTO taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
    }

    @Test
    public void findAll() {
        @NotNull final String url = API_URL + "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<TaskDTO[]> response =
                (template.exchange(url, HttpMethod.GET, new HttpEntity<>(header), TaskDTO[].class));
        Assert.assertNotNull(response.getBody());
        TaskDTO[] tasks = response.getBody();
        Assert.assertTrue(Arrays.stream(tasks).anyMatch(p -> task_1.getId().equals(p.getId())));
    }

    @Test
    public void count() {
        @NotNull final String url = API_URL + "count";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<Long> response =
                template.exchange(url, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(3L, response.getBody().longValue());
    }

    @Test
    public void save() {
        @NotNull final String url = API_URL + "save";
        @NotNull final String taskId = task_1.getId();
        @NotNull final String expected = "NEW NAME";
        task_1.setName(expected);
        sendRequest(url, HttpMethod.POST, new HttpEntity<TaskDTO>(task_1, header));
        @NotNull final String findUrl = API_URL + "findById/" + taskId;
        @NotNull final ResponseEntity<TaskDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDTO taskDto = response.getBody();
        Assert.assertEquals(expected, taskDto.getName());
    }

    @Test
    public void delete() {
        @NotNull final String url = API_URL + "delete";
        @NotNull final String taskId = task_1.getId();
        @NotNull final String findUrl = API_URL + "findById/" + taskId;
        @NotNull ResponseEntity<TaskDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDTO taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<TaskDTO>(task_1, header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteById() {
        @NotNull final String taskId = task_1.getId();
        @NotNull final String url = API_URL + "deleteById/" + taskId;
        @NotNull final String findUrl = API_URL + "findById/" + taskId;
        @NotNull ResponseEntity<TaskDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDTO taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteAll() {
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        tasks.add(task_1);
        @NotNull final String taskId = task_1.getId();
        @NotNull final String url = API_URL + "deleteAll";
        @NotNull final String findUrl = API_URL + "findById/" + taskId;
        @NotNull ResponseEntity<TaskDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDTO taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(tasks, header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void clear() {
        @NotNull final String taskId = task_1.getId();
        @NotNull final String url = API_URL + "clear";
        @NotNull final String findUrl = API_URL + "findById/" + taskId;
        @NotNull final String findAllUrl = API_URL + "findAll";
        @NotNull ResponseEntity<TaskDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final TaskDTO taskDto = response.getBody();
        Assert.assertEquals(taskId, taskDto.getId());
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<TaskDTO[]> responseEntity =
                template.exchange(findAllUrl, HttpMethod.GET, new HttpEntity<>(header), TaskDTO[].class);
        Assert.assertNotNull(responseEntity.getBody());
        int countTasks = responseEntity.getBody().length;
        Assert.assertEquals(0, countTasks);
    }

}
