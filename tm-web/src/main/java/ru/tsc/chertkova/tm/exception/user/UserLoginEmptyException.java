package ru.tsc.chertkova.tm.exception.user;

public class UserLoginEmptyException extends AbstractUserException {

    public UserLoginEmptyException() {
        super("Ошибка! Логин пользователя не задан.");
    }

}
