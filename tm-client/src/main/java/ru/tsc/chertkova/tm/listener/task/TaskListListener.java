package ru.tsc.chertkova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.task.TaskListRequest;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractTaskListener;
import ru.tsc.chertkova.tm.model.Task;

import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Show task list.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@taskListListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[TASK LIST]");
        //System.out.println("[ENTER SORT:]");
        //System.out.println(Arrays.toString(Sort.values()));
        //@Nullable final String sortType = TerminalUtil.nextLine();
        //@Nullable final Sort sort = Sort.toSort(sortType);
        @Nullable final List<Task> tasks = getTaskEndpoint()
                .listTask(new TaskListRequest(getToken())).getTasks();
        renderTasks(tasks);
    }

}
