package ru.tsc.chertkova.tm.integration.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;

import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.chertkova.tm.marker.WebCategory;
import ru.tsc.chertkova.tm.model.dto.ProjectDTO;
import ru.tsc.chertkova.tm.model.dto.Result;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(WebCategory.class)
public class ProjectRestEndpointClientTest {

    @NotNull
    private static final String API_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @NotNull
    private static String SESSION_ID;

    @NotNull
    private ProjectDTO project_1;

    @NotNull
    private ProjectDTO project_2;

    @NotNull
    private ProjectDTO project_3;

    @NotNull
    private ProjectDTO project_4;

    @BeforeClass
    @SneakyThrows
    public static void setUpClass() {
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final String authUrl = "http://localhost:8080/api/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response = template.getForEntity(authUrl, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders responseHeaders = response.getHeaders();
        final List<HttpCookie> cookies =
                HttpCookie.parse(responseHeaders.getFirst(HttpHeaders.SET_COOKIE));
        SESSION_ID = cookies.stream()
                .filter(httpCookie -> "JSESSIONID".equals(httpCookie.getName()))
                .findFirst()
                .get()
                .getValue();
        Assert.assertNotNull(SESSION_ID);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + SESSION_ID));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<ProjectDTO> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity entity
    ) {
        @NotNull final RestTemplate template = new RestTemplate();
        return template.exchange(url, method, entity, ProjectDTO.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
    }

    @Before
    public void setUp() {
        @NotNull final String url = API_URL + "save";
        project_1 = new ProjectDTO();
        project_2 = new ProjectDTO();
        project_3 = new ProjectDTO();
        project_4 = new ProjectDTO();
        project_1.setName("PROJ_1");
        project_2.setName("PROJ_2");
        project_3.setName("PROJ_3");
        project_4.setName("PROJ_4");
        sendRequest(url, HttpMethod.POST, new HttpEntity<ProjectDTO>(project_1, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<ProjectDTO>(project_2, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<ProjectDTO>(project_3, header));

    }

    @After
    public void tearDown() throws Exception {
        @NotNull final String clearUrl = API_URL + "clear";
        sendRequest(clearUrl, HttpMethod.DELETE, new HttpEntity<>(header));
    }

    @Test
    public void create() {
        @NotNull final String url = API_URL + "create";
        @NotNull final ResponseEntity<ProjectDTO> response = sendRequest(
                url,
                HttpMethod.POST,
                new HttpEntity<>(header)
        );
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @NotNull final ProjectDTO projectDto = response.getBody();
        Assert.assertNotNull(projectDto);
    }

    @Test
    public void existsById() {
        @NotNull final String projectId = project_1.getId();
        @NotNull final String url = API_URL + "existsById/" + projectId;
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<Result> response = template.exchange(
                url,
                HttpMethod.GET,
                new HttpEntity<>(header),
                Result.class);
        Assert.assertNotNull(response.getBody());
        boolean exists = response.getBody().getSuccess();
        Assert.assertTrue(exists);
    }

    @Test
    public void findById() {
        @NotNull final String projectId = project_1.getId();
        @NotNull final String url = API_URL + "findById/" + projectId;
        @NotNull final ResponseEntity<ProjectDTO> response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDTO projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
    }

    @Test
    public void findAll() {
        @NotNull final String url = API_URL + "findAll";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<ProjectDTO[]> response =
                (template.exchange(url, HttpMethod.GET, new HttpEntity<>(header), ProjectDTO[].class));
        Assert.assertNotNull(response.getBody());
        ProjectDTO[] projects = response.getBody();
        Assert.assertTrue(Arrays.stream(projects).anyMatch(p -> project_1.getId().equals(p.getId())));
    }

    @Test
    public void count() {
        @NotNull final String url = API_URL + "count";
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<Long> response =
                template.exchange(url, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(3L, response.getBody().longValue());
    }

    @Test
    public void save() {
        @NotNull final String url = API_URL + "save";
        @NotNull final String projectId = project_1.getId();
        @NotNull final String expected = "NEW NAME";
        project_1.setName(expected);
        sendRequest(url, HttpMethod.POST, new HttpEntity<ProjectDTO>(project_1, header));
        @NotNull final String findUrl = API_URL + "findById/" + projectId;
        @NotNull final ResponseEntity<ProjectDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDTO projectDto = response.getBody();
        Assert.assertEquals(expected, projectDto.getName());
    }

    @Test
    public void delete() {
        @NotNull final String url = API_URL + "delete";
        @NotNull final String projectId = project_1.getId();
        @NotNull final String findUrl = API_URL + "findById/" + projectId;
        @NotNull ResponseEntity<ProjectDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDTO projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<ProjectDTO>(project_1, header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteById() {
        @NotNull final String projectId = project_1.getId();
        @NotNull final String url = API_URL + "deleteById/" + projectId;
        @NotNull final String findUrl = API_URL + "findById/" + projectId;
        @NotNull ResponseEntity<ProjectDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDTO projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteAll() {
        @NotNull final List<ProjectDTO> projects = new ArrayList<>();
        projects.add(project_1);
        @NotNull final String projectId = project_1.getId();
        @NotNull final String url = API_URL + "deleteAll";
        @NotNull final String findUrl = API_URL + "findById/" + projectId;
        @NotNull ResponseEntity<ProjectDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDTO projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(projects, header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
    }

    @Test
    public void clear() {
        @NotNull final String projectId = project_1.getId();
        @NotNull final String url = API_URL + "clear";
        @NotNull final String findUrl = API_URL + "findById/" + projectId;
        @NotNull final String findAllUrl = API_URL + "findAll";
        @NotNull ResponseEntity<ProjectDTO> response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNotNull(response.getBody());
        @NotNull final ProjectDTO projectDto = response.getBody();
        Assert.assertEquals(projectId, projectDto.getId());
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header));
        response = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header));
        Assert.assertNull(response.getBody());
        @NotNull final RestTemplate template = new RestTemplate();
        @NotNull final ResponseEntity<ProjectDTO[]> responseEntity =
                template.exchange(findAllUrl, HttpMethod.GET, new HttpEntity<>(header), ProjectDTO[].class);
        Assert.assertNotNull(responseEntity.getBody());
        int countProjects = responseEntity.getBody().length;
        Assert.assertEquals(0, countProjects);
    }

}
