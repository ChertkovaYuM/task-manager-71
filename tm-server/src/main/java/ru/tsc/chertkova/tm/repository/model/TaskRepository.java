package ru.tsc.chertkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository
        extends AbstractUserOwnerModelRepository<Task> {

    @Nullable
    @Query("SELECT e FROM Task e WHERE e.user.id=:userId AND e.project.id=:projectId")
    List<Task> findAllByProjectId(@Nullable @Param("userId") String userId,
                                  @Nullable @Param("projectId") String projectId);

    @Modifying
    @Query("UPDATE Task e SET e.status=:status WHERE e.user.id=:userId AND e.id=:id")
    void changeStatus(@NotNull @Param("id") String id,
                      @NotNull @Param("userId") String userId,
                      @NotNull @Param("status") String status);

    @Modifying
    @Query("UPDATE Task e SET e.project.id=:projectId WHERE e.user.id=:userId AND e.id=:taskId")
    void bindTaskToProject(@NotNull @Param("taskId") String taskId,
                           @NotNull @Param("projectId") String projectId,
                           @NotNull @Param("userId") String userId);

    @Modifying
    @Query("DELETE FROM Task e WHERE e.user.id=:userId")
    void clear(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT e FROM Task e WHERE e.user.id=:userId")
    List<Task> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Query("SELECT e FROM Task e WHERE e.id=:id AND e.user.id=:userId")
    Task findById(@NotNull @Param("userId") String userId,
                  @NotNull @Param("id") String id);

    @Query("SELECT COUNT(e) FROM Task e WHERE e.user.id=:userId")
    int getSize(@NotNull @Param("userId") String userId);

    @Modifying
    @Query("DELETE FROM Task e WHERE e.id=:id AND e.user.id=:userId")
    Task removeById(@NotNull final String userId,
                    @NotNull final String id);

}
