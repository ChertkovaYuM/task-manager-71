package ru.tsc.chertkova.tm.exception.user;

import ru.tsc.chertkova.tm.exception.AbstractException;

public class UserEmailEmptyException extends AbstractException {

    public UserEmailEmptyException() {
        super("Ошибка! Имейл пользователя не задан.");
    }

}
