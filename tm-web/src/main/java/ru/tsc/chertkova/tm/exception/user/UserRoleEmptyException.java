package ru.tsc.chertkova.tm.exception.user;

public class UserRoleEmptyException extends AbstractUserException {

    public UserRoleEmptyException() {
        super("Ошибка! Роль пользователя не задана.");
    }

}
